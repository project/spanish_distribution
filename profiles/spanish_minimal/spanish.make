; Make file for Spanish Profile.
api = 2
core = 7.x

projects[] = drupal

; Close future
; translations[] = es

; Modules
projects[] = backup_migrate
projects[captcha][version] = "1.0-beta2"
projects[] = ckeditor
projects[] = css_emimage
projects[] = ctools
projects[] = devel
projects[] = dhtml_menu
projects[] = ds
projects[] = filefield_paths
projects[] = imce
projects[] = l10n_client
projects[] = l10n_update
projects[] = libraries
projects[] = page_title
projects[] = pathauto
projects[] = entity
projects[] = entityreference
projects[] = token
projects[] = transliteration
projects[] = views
projects[] = admin_menu
projects[] = skinr
projects[] = diff
projects[] = google_analytics
projects[] = typogrify
projects[] = features
projects[] = media
projects[] = link
projects[] = context
projects[] = luxe
projects[] = date
projects[] = webform
projects[] = jquery_update
projects[] = xmlsitemap
projects[] = views_slideshow
projects[] = globalredirect
projects[] = metatag
projects[] = less
projects[] = honeypot
projects[] = nivo_slider
projects[] = mailsystem
projects[] = mimemail
; themes
projects[] = bootstrap
projects[] = tao
projects[] = rubik

; PLUPLOAD
libraries[plupload][download][type] = "get"
libraries[plupload][download][url] = "https://github.com/downloads/moxiecode/plupload/plupload_1_5_4.zip"
libraries[plupload][directory_name] = "plupload"
libraries[plupload][destination] = "libraries"

; CKEditor
libraries[ckeditor][download][type]= "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.1.1/ckeditor_4.1.1_full.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][destination] = "libraries"
